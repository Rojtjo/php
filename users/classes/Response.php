<?php

class Response {

	/**
	 * The data that will be sent to the browser.
	 *
	 * @var string
	 */
	protected $data;

	/**
	 * The headers that will be sent with the response.
	 *
	 * @var array
	 */
	protected $headers;

	/**
	 * Create a new response object.
	 *
	 * @param array $headers
	 */
	public function __construct($data = '', $headers = array())
	{
		$this->data = $data;
		$this->headers = $headers;
	}

	/**
	 * Send the response to the browser.
	 *
	 * @return string
	 */
	public function send()
	{
		foreach($this->headers as $name => $value)
		{
			header("$name: $value");
		}

		return (string) $this->data;
	}

}
