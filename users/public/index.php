<?php

require '../boot.php';

$method = $_SERVER['REQUEST_METHOD'];
$uri    = $_SERVER['REQUEST_URI'];

// Echo the results to the browser
$response = Router::call($method, $uri);

exit($response->send());
