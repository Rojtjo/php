<?php

class Config {

	protected static $instance;

	/**
	 * Base path used for includes.
	 *
	 * @var string
	 */
	protected static $path;

	protected static $configs = array();

	/**
	 * Set the config path.
	 *
	 * @param string $path
	 */
	public static function setPath($path)
	{
		static::$path = $path;
	}

	/**
	 * Get an item from the config with dot notation.
	 *
	 * @param  string $key
	 * @param  mixed  $default
	 * @return mixed
	 */
	public static function get($key, $default = null)
	{
		$array = static::getFile($key);

		if (empty($key)) return $array;

		if (isset($array[$key])) return $array[$key];

		foreach (explode('.', $key) as $segment)
		{
			if ( ! is_array($array) || ! array_key_exists($segment, $array))
			{
				return is_callable($default) ? $default() : $default;
			}

			$array = $array[$segment];
		}

		return $array;
	}

	protected static function exists($path)
	{
		return file_exists($path);
	}

	protected static function getFile(&$key)
	{
		$parts = explode('.', $key);
		$file  = array_shift($parts);

		if( ! isset(static::$configs[$file]))
		{
			$path = realpath(static::$path.$file.'.php');
			static::$configs[$file] = require $path;
		}

		$key = implode('.', $parts);

		return static::$configs[$file];
	}

}
