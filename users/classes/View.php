<?php

class View {

	/**
	 * View file path.
	 *
	 * @var string
	 */
	protected $view;

	/**
	 * View parameters.
	 *
	 * @var array
	 */
	protected $parameters;

	/**
	 * Base path used for includes.
	 *
	 * @var string
	 */
	protected static $path;

	/**
	 * Create a new view instance.
	 *
	 * @param string $view
	 * @param array  $parameters
	 */
	public function __construct($view, $parameters = array())
	{
		$this->view       = $view;
		$this->parameters = $parameters;
	}

	/**
	 * Set the view path.
	 *
	 * @param string $path
	 */
	public static function setPath($path)
	{
		static::$path = $path;
	}

	/**
	 * Create a new view instance.
	 *
	 * @param  string $view
	 * @param  array  $parameters
	 * @return string
	 */
	public static function make($view, $parameters = array())
	{
		return new static($view, $parameters);
	}

	/**
	 * Include extra parameters in the view.
	 *
	 * @param  string $key
	 * @param  string $value
	 * @return this
	 */
	public function with($key, $value)
	{
		$this->parameters[$key] = $value;

		return $this;
	}

	/**
	 * Render the view.
	 *
	 * @return string
	 */
	public function render()
	{
		if($this->exists())
		{
			extract($this->parameters);
			ob_start();
			include $this->getViewPath();

			return ob_get_clean();
		}

		return '';
	}

	/**
	 * Determine if the view exists.
	 *
	 * @return bool
	 */
	public function exists()
	{
		$path = $this->getViewPath();

		return file_exists($path);
	}

	/**
	 * Get the view path from filename and basepath.
	 *
	 * @return string
	 */
	public function getViewPath()
	{
		$view = str_replace('.', DIRECTORY_SEPARATOR, $this->view);

		return realpath(static::$path.$view.'.php');
	}

	/**
	 * Get the view as string.
	 *
	 * @return string
	 */
	public function __toString()
	{
		return $this->render();
	}

}
