<?php

Router::get('/', function()
{
	$users = DB::query('SELECT * FROM users');

	$content = View::make('users.index', compact('users'));
	return View::make('layout', compact('content'));
});

Router::get('edit/(:num)', function($id)
{
	$user = DB::prepare('SELECT * FROM users WHERE id = ?', array($id));
	$user = $user[0];
	$content = View::make('users.edit');
	return View::make('layout', compact('content'));
});

Router::get('create', function()
{
	$content = View::make('users.create');
	return View::make('layout', compact('content'));
});

Router::post('create', function()
{

});

Router::get('login', function()
{
	$content = View::make('auth.login');
	return View::make('layout', compact('content'));
});

Router::post('login', function()
{

	$email    = $_POST['email'];
	$password = $_POST['password'];

	$results = DB::prepare('SELECT * FROM users WHERE email = ?', array($email));

	if(count($results) > 0)
	{
		// 1 of meer gebruikers gevonden
		$user = $results[0];

		if($user['password'] === $password)
		{
			// Gebruiker geidentificeerd
			$_SESSION['user'] = array(
				'id'         => $user['id'],
				'first_name' => $user['first_name'],
				'last_name'  => $user['last_name']
			);

			return Redirect::to('/');
		}
	}

	$_SESSION['message'] = 'Uw gebruikersnaam en/of wachtwoord is incorrect.';

	return Redirect::to('/login');
});

Router::get('logout', function()
{
	session_destroy();

	return Redirect::to('/login');
});

Router::get('secret', function()
{
	if(isset($_SESSION['user']))
	{
		return View::make('secret');
	}

	$_SESSION['message'] = 'Dit is een beveiligde pagina!';

	return Redirect::to('login');
});