<?php

class DB {

	/**
	 * The pdo connection instance.
	 *
	 * @var \PDO
	 */
	protected static $pdo;

	/**
	 * Setup the pdo connection.
	 *
	 * @param  array $config
	 * @return void
	 */
	public static function setup($config)
	{
		static::$pdo = static::createPdoConnection($config);
		static::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	/**
	 * Create a new PDO instance.
	 *
	 * @param  array $config
	 * @return \PDO
	 */
	protected static function createPdoConnection($config)
	{
		extract($config);

		return new PDO("mysql:host=$host;dbname=$database", $username, $password);
	}

	/**
	 * Prepare a PDO statement.
	 *
	 * @param  string $sql
	 * @return \PDOStatement
	 */
	public static function prepare($sql, $parameters = array())
	{
		$stmt = static::$pdo->prepare($sql);
		$stmt->setFetchMode(PDO::FETCH_ASSOC);
		$stmt->execute($parameters);

		return $stmt->fetchAll();
	}

	/**
	 * Send a query to the PDO instance.
	 *
	 * @param  string $sql
	 * @return array
	 */
	public static function query($sql)
	{
		return static::$pdo->query($sql, PDO::FETCH_ASSOC)->fetchAll();
	}

}
