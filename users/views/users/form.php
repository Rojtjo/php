<div class="form-group">
	<label for="first_name">Voornaam</label>
	<input type="text" class="form-control" id="first_name" name="first_name">
</div>

<div class="form-group">
	<label for="last_name">Achternaam</label>
	<input type="text" class="form-control" id="last_name" name="last_name">
</div>

<div class="form-group">
	<label for="email">Email</label>
	<input type="email" class="form-control" id="email" name="email">
</div>

<div class="form-group">
	<label for="password">Password</label>
	<input type="password" class="form-control" id="password" name="password">
</div>

<button type="submit" class="btn btn-primary">Opslaan</button>
<button type="reset" class="btn btn-default">Reset</button>
