<div class="btn-toolbar">
	<a href="/create" class="btn btn-primary">Create</a>
</div>
<table class="table">
	<thead>
		<tr>
			<th>ID</th>
			<th>Email</th>
			<th>Voornaam</th>
			<th>Achternaam</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach($users as $user): ?>
			<tr>
				<td><?php echo $user['id'] ?></td>
				<td><?php echo $user['email'] ?></td>
				<td><?php echo $user['first_name'] ?></td>
				<td><?php echo $user['last_name'] ?></td>
			</tr>
		<?php endforeach;?>
	</tbody>
</table>
