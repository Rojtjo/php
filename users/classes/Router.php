<?php

class Router {

	/**
	 * Registered routes.
	 *
	 * @var array
	 */
	protected static $routes = array(
		'GET'  => array(),
		'POST' => array()
	);

	/**
	 * The wildcard patterns supported by the router.
	 *
	 * @var array
	 */
	protected static $patterns = array(
		'(:num)'     => '([0-9]+)',
		'(:any)'     => '([a-zA-Z0-9\.\-_%=]+)',
		'(:segment)' => '([^/]+)',
		'(:all)'     => '(.*)'
	);

	/**
	 * Register a new route.
	 *
	 * @param  string $method
	 * @param  string $pattern
	 * @param  string $action
	 * @return void
	 */
	public static function register($method, $pattern, Closure $action)
	{
		$pattern = static::createRegexPattern($pattern);
		static::$routes[$method][$pattern] = $action;
	}

	/**
	 * Create a regex pattern from the given string.
	 *
	 * @param  string $pattern
	 * @return string
	 */
	protected static function createRegexPattern($pattern)
	{
		$search  = array_keys(static::$patterns);
		$replace = array_values(static::$patterns);
		$pattern = str_replace($search, $replace, $pattern);

		return '/'.trim($pattern, '/');
	}

	/**
	 * Register alias.
	 *
	 * @param  string $pattern
	 * @param  string $action
	 * @return string
	 */
	public static function get($pattern, $action)
	{
		return static::register('GET', $pattern, $action);
	}

	/**
	 * Register alias.
	 *
	 * @param  string $pattern
	 * @param  string $action
	 * @return string
	 */
	public static function post($pattern, $action)
	{
		return static::register('POST', $pattern, $action);
	}

	/**
	 * Find a matching route and call it.
	 *
	 * @param  string $method
	 * @param  string $uri
	 * @return void
	 */
	public static function call($method, $uri)
	{
		if(isset(static::$routes[$method][$uri]))
		{
			$action = static::$routes[$method][$uri];

			return static::wrapResponse($action());
		}

		foreach(static::$routes[$method] as $pattern => $action)
		{
			if(preg_match('#^'.$pattern.'$#u', $uri, $parameters))
			{
				$data = call_user_func_array($action, array_slice($parameters, 1));
				return static::wrapResponse($action);
			}
		}

		return static::wrapResponse('De opgevraagde pagina kon niet worden gevonden.');
	}

	/**
	 * Wrap the given returned data in a Response object.
	 *
	 * @param  array $data
	 * @return Response
	 */
	protected static function wrapResponse($data)
	{
		if($data instanceof Response)
		{
			return $data;
		}

		return new Response($data);
	}

}
