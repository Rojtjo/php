<?php
session_start();

// Require the database class
require 'classes/Config.php';
require 'classes/DB.php';
require 'classes/Response.php';
require 'classes/Redirect.php';
require 'classes/Router.php';
require 'classes/Response.php';
require 'classes/Redirect.php';
require 'classes/View.php';

Config::setPath(__DIR__.'/config/');
View::setPath(__DIR__.'/views/');
DB::setup(Config::get('database'));


require '../routes.php';
