<?php if(isset($_SESSION['message'])): ?>
	<div>
		<h2><?=$_SESSION['message']?></h2>
	</div>
<?php endif;?>

<form action="/login" method="post">

	<div>
		<label for="email">Email</label>
		<input type="text" name="email">
	</div>

	<div>
		<label for="password">Wachtwoord</label>
		<input type="password" name="password">
	</div>

	<button type="submit">Login</button>

</form>