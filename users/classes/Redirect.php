<?php

class Redirect extends Response {

	/**
	 * Redirect to the given path.
	 *
	 * @param  string $path
	 * @return Redirect
	 */
	public function to($path)
	{
		return new static('', array(
			'Location' => $path
		));
	}

}
